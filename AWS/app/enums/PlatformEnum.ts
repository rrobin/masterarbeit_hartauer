export enum Platform {
  GCP = 'GCP',
  AWS = 'AWS',
  AZURE = 'AZURE',
}
