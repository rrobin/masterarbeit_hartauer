provider "aws" {
   region = "us-east-2"
}
provider "archive" {}


resource "random_id" "id" {
  keepers = {
    timestamp = "${timestamp()}" # force change on every execution
  }
  byte_length = 8
}


resource "aws_dynamodb_table" "dynamo_db_table" {
  name             = var.db_table_name 
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key = var.db_user_table_hash_key
  attribute {
    name = "userId"
    type = "S"
  }
}

resource "aws_s3_bucket" "bucket_lambda_tf_portabel" {
  bucket = var.bucket
  acl    = "private"   # or can be "public-read"
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

data "archive_file" "lambda_archive" {
  source_dir  = var.archive_source_dir
  output_path = "./dist/${resource.random_id.id.dec}.zip"
  type        = "zip"
}

# Upload an object
resource "aws_s3_bucket_object" "s3_zip_lambda_portabel" {
  bucket = aws_s3_bucket.bucket_lambda_tf_portabel.id
  key    = var.upload_bucket_key
  acl    = "private"  # or can be "public-read"
  source = "./dist/${resource.random_id.id.dec}.zip"
  # etag = filemd5("./dist/dist.zip")
  depends_on = [
    data.archive_file.lambda_archive,

  ]
}


resource "aws_iam_role_policy" "write_policy" {
  name = "lambda_write_policy"
  role = aws_iam_role.writeRole.id

  policy = file("./policies/write_policy.json")
}

resource "aws_iam_role" "writeRole" {
  name = "myWriteRole"
  assume_role_policy = file("./policies/assume_write_role_policy.json")
}


resource "aws_lambda_function" "writeLambda" {

  function_name = "writeLambda"
  s3_bucket     = aws_s3_bucket.bucket_lambda_tf_portabel.id
  s3_key        = var.upload_bucket_key
  role          = aws_iam_role.writeRole.arn
  handler       = var.handler
  runtime       = "nodejs14.x"
  environment {
    variables = {
      USERS_TABLE = "users-table"
      PLATFORM ="AWS"
    }
  }
  depends_on = [
    aws_iam_role_policy_attachment.lambda_logs,
    aws_cloudwatch_log_group.example,
    aws_s3_bucket_object.s3_zip_lambda_portabel
  ]
}

resource "aws_lambda_permission" "apigw" {
   statement_id  = "AllowAPIGatewayInvoke"
   action        = "lambda:InvokeFunction"
   function_name = aws_lambda_function.writeLambda.function_name
   principal     = "apigateway.amazonaws.com"

   # The "/*/*" portion grants access from any method on any resource
   # within the API Gateway REST API.
   source_arn = "${aws_api_gateway_rest_api.demo.execution_arn}/*/*"
}

resource "aws_cloudwatch_log_group" "example" {
  name              = "/aws/lambda/writeLambda"
  retention_in_days = 5
}

resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.writeRole.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

output "base_url" {
  value = aws_api_gateway_deployment.gatewayDemo.invoke_url
}

