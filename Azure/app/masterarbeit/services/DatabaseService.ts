import { UserModel } from '../models/userModel';

export interface DatabaseService {
  USERS_TABLE: string;
  getUser(userId: string): Promise<UserModel>;
  putUser(user: UserModel): Promise<UserModel>;
  updateUser(user: UserModel): Promise<UserModel>;
}
