provider "google" {
  project = var.project
  region  = var.region
}

locals {
  timestamp = formatdate("YYMMDDhhmmss", timestamp())
  root_dir  = abspath("../")
}

resource "random_id" "project_id" {
  byte_length = 4
}

resource "random_id" "zipId" {
  keepers = {
    timestamp = "${timestamp()}" # force change on every execution
  }
  byte_length = 8
}
resource "random_id" "storage_id" {
  byte_length = 4
}


# Compress source code
data "archive_file" "source_archive" {
  source_dir  = var.archive_source_dir
  output_path = "./dist/${resource.random_id.zipId.dec}.zip"
  type        = "zip"
}

# resource "google_project" "my_project" {
#   name       = lower(var.project)
#   project_id = lower("${var.project}${random_id.project_id.id}")
# }

# Create bucket that will host the source code
resource "google_storage_bucket" "bucket" {
  name = "${var.project}-masterarbeit"
 location   = "us-central1"
}

# Add source code zip to bucket
resource "google_storage_bucket_object" "zip" {
  # Append file MD5 to force bucket to be recreated
  name   = "${var.project}test${random_id.storage_id.hex}"
  bucket = google_storage_bucket.bucket.name
  source = data.archive_file.source_archive.output_path
}

# Enable Cloud Functions API
resource "google_project_service" "cf" {
  project = var.project
  service = "cloudfunctions.googleapis.com"

  disable_dependent_services = true
  disable_on_destroy         = true
}

# Enable Cloud Build API
resource "google_project_service" "cb" {
  project = var.project
  service = "cloudbuild.googleapis.com"

  disable_dependent_services = true
  disable_on_destroy         = true
}
resource "google_project_service" "firestore" {
  project = var.project
  service = "firestore.googleapis.com"

  disable_dependent_services = true
  disable_on_destroy         = true
}

resource "google_app_engine_application" "app_engine" {
  project       = var.project
  location_id   = "us-central"
  database_type = "CLOUD_FIRESTORE"
}

# Create Cloud Function
resource "google_cloudfunctions_function" "function" {
  name    = var.function_name
  runtime = "nodejs14" # Switch to a different runtime if needed
  
  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.bucket.name
  source_archive_object = google_storage_bucket_object.zip.name
  trigger_http          = true
  entry_point           = var.function_entry_point
  environment_variables = {
    PLATFORM = "GCP"
  }
   depends_on = [
    google_project_service.cf,
  ]
  
}

# Create IAM entry so all users can invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.function.project
  region         = google_cloudfunctions_function.function.region
  cloud_function = google_cloudfunctions_function.function.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}

output "function_url" {
  # Access the module output with module.<module_name>.<output_name>
  value = google_cloudfunctions_function.function.https_trigger_url
}
