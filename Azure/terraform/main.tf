terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      # Root module should specify the maximum provider version
      # The ~> operator is a convenient shorthand for allowing only patch releases within a specific minor release.
      version = "2.82.0"
    }
  }
}

provider "azurerm" {
  features {}
}

provider "archive" {}


resource "random_id" "id" {
  keepers = {
    timestamp = "${timestamp()}" # force change on every execution
  }
  byte_length = 8
}

resource "random_id" "storage_id" {
  byte_length = 4
}

resource "random_id" "function_id" {
  byte_length = 4
}

resource "random_id" "db_resource_Id" {
  byte_length = 4
}

data "archive_file" "source_archive" {
  source_dir  = var.archive_source_dir
  output_path = "./dist/${resource.random_id.id.dec}.zip"
  type        = "zip"
}

#Resource-Group
resource "azurerm_resource_group" "resource_group" {
  name     = "${var.project}-${var.environment}-resource-group2"
  location = var.location
}

resource "azurerm_storage_account" "storage_account" {
  name                     = "${var.project}${var.environment}${random_id.storage_id.hex}"
  resource_group_name      = azurerm_resource_group.resource_group.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

# resource "azurerm_storage_container" "deployments" {
#  name                  = "function-releases"
#  storage_account_name  = azurerm_storage_account.storage_account.name
#  container_access_type = "private"
# }

#resource "azurerm_storage_blob" "appcode" {
#  name                   = "functionAppMasterarbeit.zip"
#  storage_account_name   = azurerm_storage_account.storage_account.name
#  storage_container_name = azurerm_storage_container.deployments.name
#  type                   = "Block"
#  source                 = "./dist/${resource.random_id.id.dec}.zip"
#  depends_on = [
#    azurerm_storage_container.deployments
#  ]
#}

resource "random_integer" "ri" {
  min = 10000
  max = 99999
}

resource "azurerm_cosmosdb_account" "db_account" {
  name                = "tfex-cosmos-db-${random_id.db_resource_Id.dec}"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name
  offer_type          = "Standard"
  kind                = "GlobalDocumentDB"

  enable_automatic_failover = true

  consistency_policy {
    consistency_level       = "BoundedStaleness"
    max_interval_in_seconds = 10
    max_staleness_prefix    = 200
  }

  geo_location {
    location          = azurerm_resource_group.resource_group.location
    failover_priority = 0
  }
}

resource "azurerm_cosmosdb_sql_database" "cosmos_sql_database" {
  name                = "tfex-cosmos-mongo-db"
  resource_group_name = azurerm_resource_group.resource_group.name
  account_name        = azurerm_cosmosdb_account.db_account.name
  throughput          = 400
}

resource "azurerm_app_service_plan" "app_service_plan" {
  name                = "azure-functions-test-service-plan3"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name


  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

#data "azurerm_storage_account_blob_container_sas" "storage_account_blob_container_sas" {
#  connection_string = azurerm_storage_account.storage_account.primary_connection_string
#  container_name    = azurerm_storage_container.deployments.name

#  start  = "2021-01-01T00:00:00Z"
#  expiry = "2022-01-01T00:00:00Z"

#  permissions {
#    read   = true
#    add    = false
#    create = false
#    write  = false
#    delete = false
#    list   = false
#  }
#}


resource "azurerm_application_insights" "application_insights" {
  name                = "${var.project}-${var.environment}-application-insights2"
  location            = var.location
  resource_group_name = azurerm_resource_group.resource_group.name
  application_type    = "Node.JS"
}


resource "azurerm_function_app" "function_app" {
  name                       = "masterarbeit${random_id.function_id.dec}"
  location                   = azurerm_resource_group.resource_group.location
  resource_group_name        = azurerm_resource_group.resource_group.name
  app_service_plan_id        = azurerm_app_service_plan.app_service_plan.id
  storage_account_name       = azurerm_storage_account.storage_account.name
  storage_account_access_key = azurerm_storage_account.storage_account.primary_access_key
  version                    = "~3"

  app_settings = {
    "FUNCTIONS_WORKER_RUNTIME"       = "node",
    "WEBSITE_NODE_DEFAULT_VERSION"   = "~14",
    "PLATFORM"                       = "AZURE",
    "WEBSITE_RUN_FROM_PACKAGE"       = ""
    "APPINSIGHTS_INSTRUMENTATIONKEY" = "${azurerm_application_insights.application_insights.instrumentation_key}"
    "KEY"                            = "${azurerm_cosmosdb_account.db_account.primary_key}",
    "ENDPOINT"                       = "${azurerm_cosmosdb_account.db_account.endpoint}",
    "DATABASE_NAME"                  = "${azurerm_cosmosdb_sql_database.cosmos_sql_database.name}"
  }
}

locals {
  publish_code_command = "az webapp deployment source config-zip --resource-group ${azurerm_resource_group.resource_group.name} --name ${azurerm_function_app.function_app.name} --src ${data.archive_file.source_archive.output_path}"
}

resource "null_resource" "function_app_publish" {
  provisioner "local-exec" {
    command = local.publish_code_command
  }
  depends_on = [local.publish_code_command]
  triggers = {
    input_json           = filemd5(data.archive_file.source_archive.output_path)
    publish_code_command = local.publish_code_command
  }
}




output "base_url" {
  value = azurerm_function_app.function_app.default_hostname
}

