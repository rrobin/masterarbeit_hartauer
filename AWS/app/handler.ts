const serverless = require('serverless-http');
const controller = require('./controller/userController');
module.exports.handler = serverless(controller);
