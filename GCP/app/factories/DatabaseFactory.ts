import { Platform } from '../enums/PlatformEnum';
import { FirestoreDatabaseService } from '../services/FirestoreDatabaseService';
// import { DynamoDatabaseService } from '../services/DynamoDatabaseService';
// import { CosmosDatabaseService } from '../services/CosmosDatabaseService';

export class DatabaseFactory {
  create(platform: string) {
    console.error('Platform: ' + platform);
    switch (platform) {
      // case Platform.AWS:
      //   return new DynamoDatabaseService();
      case Platform.GCP:
        return new FirestoreDatabaseService();
      // case Platform.AZURE:
      // return new CosmosDatabaseService();
    }
  }
}
