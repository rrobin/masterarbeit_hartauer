import { Platform } from '../enums/PlatformEnum';

import { DynamoDatabaseService } from '../services/DynamoDatabaseService';

export class DatabaseFactory {
  create(platform: string) {
    console.error('Platform: ' + platform);
    switch (platform) {
      case Platform.AWS:
        return new DynamoDatabaseService();
      // case Platform.GCP:
      //   return new FirestoreDatabaseService();
      // case Platform.AZURE:
      //   throw new Error(platform + ' not implemented yet');
    }
  }
}
