# variable "db_table_name" {
#     type = string
#     description = "name of database-table"
# }
# variable "db_user_table_hash_key" {
#     type = string
#     description = "name of attribute for hashkey"
# }
# variable "bucket" {
#     type = string
#     description = "name of attribute of Data-Bucket"
# }
# variable "upload_bucket_key" {
#     type = string
#     description = "key for upload bucket"
# }
# variable "handler" {
#     type = string
#     description = "key for upload bucket"
# }

variable "archive_source_dir" {
  type        = string
  description = "source directory for archive"
}
variable "environment" {
  type        = string
  description = "Deployment Enviroment"
}
variable "project" {
  type        = string
  description = "GCP-Cloud-Project"
}
variable "function_name" {
  type        = string
  description = "GCP-Cloud-Functionsname"
}
variable "function_entry_point" {
  type        = string
  description = "GCP-Cloud-Functions-EntryPoint"
}
variable "region" {
  type        = string
  description = "GCP-Cloud-Region"
}

