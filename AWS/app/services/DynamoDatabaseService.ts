import { UserModel } from '../models/userModel';
import { DatabaseService } from './DatabaseService';
const AWS = require('aws-sdk');
const dynamoDbClient = new AWS.DynamoDB.DocumentClient();
export class DynamoDatabaseService implements DatabaseService {
  USERS_TABLE: string;

  constructor() {
    this.USERS_TABLE = process.env.USERS_TABLE;
  }

  async getUser(userId: string): Promise<UserModel> {
    const params = {
      TableName: this.USERS_TABLE,
      Key: {
        userId: userId,
      },
    };
    const { Item } = await dynamoDbClient.get(params).promise();
    return new UserModel(Item.userId, Item.name);
  }
  async putUser(user: UserModel): Promise<UserModel> {
    const params = {
      TableName: this.USERS_TABLE,
      Item: {
        userId: user.id,
        name: user.name,
      },
    };
    await dynamoDbClient.put(params).promise();
    return user;
  }
  updateUser(user: UserModel): Promise<UserModel> {
    console.log('Called not implemendet update function');
    throw new Error('Method not implemented.');
  }
}
export * as utilities from './DynamoDatabaseService';
