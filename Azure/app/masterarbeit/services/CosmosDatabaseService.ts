import { UserModel } from '../models/userModel';
import { DatabaseService } from './DatabaseService';
import { v4 as uuidv4 } from 'uuid';
import { env } from 'process';

// https://docs.microsoft.com/en-us/javascript/api/overview/azure/cosmos-readme?view=azure-node-latest
const { CosmosClient } = require('@azure/cosmos');

export class CosmosDatabaseService implements DatabaseService {
  USERS_TABLE: string;
  container: any;
  database: any;

  endpoint = process.env.ENDPOINT;
  key = process.env.KEY;
  client = new CosmosClient({ endpoint: this.endpoint, key: this.key });

  async getUser(userId: string): Promise<any> {
    await this.init();

    const querySpec = {
      query: 'SELECT * from c WHERE c.id = ' + "'" + userId + "'",
    };
    const { resources: items } = await this.container.items.query(querySpec).fetchAll();

    const user = new UserModel(items[0].id, items[0].name);
    return user;
  }
  async putUser(user: UserModel): Promise<UserModel> {
    await this.init();
    user.id = uuidv4();
    this.container.items.create(user);
    return user;
  }
  updateUser(user: any): Promise<any> {
    console.log('Called not implemendet update function');
    throw new Error('Method not implemented.');
  }

  public async init() {
    const resultdb = await this.client.databases.createIfNotExists({ id: process.env.DATABASE_NAME });
    this.database = resultdb.database;
    const resultContainer = await this.database.containers.createIfNotExists({ id: 'masterarbeitDBContainer' });
    this.container = resultContainer.container;
  }
}
