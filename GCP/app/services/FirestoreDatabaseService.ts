import { UserModel } from '../models/userModel';
import { DatabaseService } from './DatabaseService';
import * as admin from 'firebase-admin';
import { stringify } from 'querystring';
admin.initializeApp({
  credential: admin.credential.applicationDefault(),
});
const db = admin.firestore();

const COLLECTION_NAME = 'faas_portability';

export class FirestoreDatabaseService implements DatabaseService {
  USERS_TABLE: string;
  async getUser(userId: string): Promise<UserModel> {
    const usersRef = db.collection(COLLECTION_NAME);
    const snapshot = await usersRef.where('id', '==', userId).get();

    if (snapshot.empty) {
      console.log('No matching documents.');
      return;
    }
    var userModel = new UserModel('123', 'test');
    snapshot.forEach((doc) => {
      userModel = new UserModel(doc.data().id, doc.data().name);
    });
    return userModel;
  }

  async putUser(user: UserModel): Promise<UserModel> {
    const created = new Date().getTime();
    const id = user.id;
    const name = user.name;
    return db
      .collection(COLLECTION_NAME)
      .add({
        created,
        id,
        name,
      })
      .then((doc) => {
        return user;
      })
      .catch((err) => {
        throw err;
      });
  }
  async updateUser(user: UserModel): Promise<UserModel> {
    console.log('Called not implemendet update function');
    throw new Error('Method not implemented.');
  }
}
