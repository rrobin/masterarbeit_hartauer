const azureFunctionHandler = require('azure-aws-serverless-express');
const app = require('./controller/userController');
const MiddlewareHandler = require('azure-middleware');
const intercept = require('azure-function-log-intercept');

const ChainedFunction = new MiddlewareHandler()
  .use((ctx) => {
    Promise.resolve(1).then(() => {
      ctx.log.info('Im called second');
      intercept(ctx);
      ctx.next();
    });
  })
  .use(azureFunctionHandler(app))
  .listen();

module.exports.handler = ChainedFunction;
