const express = require('express');

import { UserModel } from '../models/userModel';
import { DatabaseFactory } from '../factories/DatabaseFactory';

const app = express();
app.use(express.json());

type ExpressRouteFunc = (req: any, res: any, next?: any) => void | Promise<void>;
export function postUserRouter(): ExpressRouteFunc {
  return async function (req: any, res: any) {
    const { userId, name } = req.body;

    if (typeof userId !== 'string') {
      res.status(400).json({ error: '"userId" must be a string' });
    } else if (typeof name !== 'string') {
      res.status(400).json({ error: '"name" must be a string' });
    }

    try {
      console.log('process.env.PLATFORM: ' + process.env.PLATFORM);
      const dataBaseFactory = new DatabaseFactory().create(process.env.PLATFORM);
      const createdDocument = await dataBaseFactory.putUser(new UserModel(userId, name));
      res.json(createdDocument);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Could not create user' });
    }
  };
}
export function getUserRoute(): ExpressRouteFunc {
  return async function (req: any, res: any) {
    try {
      console.log('Platform: ' + process.env.PLATFORM);
      const dataBaseFactory = new DatabaseFactory().create(process.env.PLATFORM);
      const foundUser = await dataBaseFactory.getUser(req.params.userId);
      if (foundUser) {
        res.json(foundUser);
      } else {
        res.status(404).json({ error: 'Could not find user with provided "userId"' });
      }
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'error occured' });
    }
  };
}

app.get('/masterarbeit/users/:userId', getUserRoute());

app.post('/masterarbeit/users', postUserRouter());

app.use((req: any, res: any, next: any) => {
  return res.status(404).json({
    error: 'Not Found',
  });
});

module.exports = app;
