variable "db_table_name" {
  type        = string
  description = "name of database-table"
}
variable "db_user_table_hash_key" {
  type        = string
  description = "name of attribute for hashkey"
}

variable "project" {
  type        = string
  description = "name of project"
}

variable "environment" {
  type        = string
  description = "name of enviroment"
}
variable "location" {
  type        = string
  description = "name of location"
}

variable "bucket" {
  type        = string
  description = "name of attribute of Data-Bucket"
}
variable "archive_source_dir" {
  type        = string
  description = "source directory for archive"
}
variable "upload_bucket_key" {
  type        = string
  description = "key for upload bucket"
}
variable "handler" {
  type        = string
  description = "key for upload bucket"
}

