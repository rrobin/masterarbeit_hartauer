# Source-Structure

- The Repository ist structured by the different cloud-provider
- [plattform name]
  - app => includes the sourcecode
  - terraform => includes the IaC-files

# Preconditions

- Terraform-CLI, Node and npm must be installed
- Terraform variables can be customized by editing the terraform.tfvars file

# Deployment-AWS

- precondition: Credetials for AWS with the necessary rights must be stored
- cd /aws/app/
- start deployment: 'npm run deploy'
- accept the infrastructure change with 'yes'
- after the successfull deployment, you can reach the following routes
  - [generierte URL]/function/masterarbeit/users
  - [generierte URL] /function/masterarbeit/users/{userId}
- 'npm run destroy' destroys the infrastructure
- accept the changes with 'yes'

# Deloyment-Azure

- to use the necessary "CosmosDB" you have to configure a billing account
- precondition: Credentials for the Azure-Cli with the correct rights must be configured.
- cd /azure/App
- start deployment: 'npm run deploy'
- accept the infrastructure change with 'yes'
- after the successfull deployment, you can reach the following routes
  - [generierte URL]/masterarbeit/users
  - [generierte URL ]/masterarbeit/users/{userId}
- 'npm run destroy' destroys the infrastructure
- accept the changes with 'yes'

# Deployment-GCP

- precondition: Credetials for GCP with the necessary rights must be stored
- cd /azure/App
- start deployment: 'npm run deploy'
- accept the infrastructure change with 'yes'
- after the successfull deployment, you can reach the following routes
  - [generierte URL]/masterarbeit/users
  - [generierte URL ]/masterarbeit/users/{userId}
- 'npm run destroy' destroys the infrastructure
- accept the changes with 'yes'

******\_\_\_\_******german version below********\_********

# Source-Aufbau

- Das Repository ist gegliedert nach den verschiedenen Cloud-Plattformen.
- Plattform
  - app => beinhaltet den Sourcecode
  - terraform => beinhaltet IaC-terraform-files

# Vorbedingungen

- Terraform-CLI, Node, npm muss installiert sein
- Terraform-Variablen können sich im File terraform.tfvars anpassen oder erweitern lassen

# Deployment-AWS

- Vorbedingung: Es müssen Zugangsdaten zur AWS-Plattform mit den entsprechenden Berechtigungen hinterlegt sein
- Wechsel in den ordner /app/
- Deployment starten über 'npm run deploy'
- Anlegen der Infrastruktur bei Nachfrage mit 'yes' bestätigen
- Bei erfolgreichem Deployment wird die URL unter der die Function erreichbar ist in der Konsole angezeigt.
  - Erreichbare Routen:
    - [generierierte URL]/function/masterarbeit/users
    - [generierte URL] /function/masterarbeit/users/{userId}
- Infrastrukutr entfernen mit 'npm run destroy'
- Bei Nachfrage, ob wirklich zerstört werden soll mit 'yes' bestätigen.
-

# Deployment-Azure

- Um die notwendige CosmosDB einzurichten muss ein Account mit Abrechnungskonto vorhanden sein
- Vorbedingung: Über die Azure-CLI müssen entsprechende Zugangsdaten hinterlegt werden. Welche Terraform für das Deployment nutzt.
- Wechsel in den ordner /app/
- Deployment starten über 'npm run deploy'
- Anlegen der Infrastruktur bei Nachfrage mit 'yes' bestätigen
- Bei erfolgreichem Deployment wird die URL unter der die Function erreichbar ist in der Konsole angezeigt.
  - Erreichbare Routen
    - [generierte URL]/masterarbeit/users
    - [generierte URL ]/masterarbeit/users/{userId}
- Infrastrukutr entfernen mit 'npm run destroy'
- Bei Nachfrage, ob wirklich zerstört werden soll mit 'yes' bestätigen.

# Deployment-GCP

- Vorbedingung: Über die Google-CLI müssen entsprechende Zugangsdaten hinterlegt werden.
- Wechsel in den ordner /app/
- Deployment starten über 'npm run deploy'
- Anlegen der Infrastruktur bei Nachfrage mit 'yes' bestätigen
- Bei erfolgreichem Deployment wird die URL unter der die Function erreichbar ist in der Konsole angezeigt.
  - Erreichbare Routen
    - [generierte URL]/masterarbeit/users
    - [generierte URL ]/masterarbeit/users/{userId}
- Infrastrukutr entfernen mit 'npm run destroy'
- Bei Nachfrage, ob wirklich zerstört werden soll mit 'yes' bestätigen.
